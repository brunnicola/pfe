Algorithme d'optimisation d'hypercubes latins par essaims particulaires
---


Commande pour lancer le programme :
---

sbt “run -dimension <dimension> \
-samples <number_of_samples> \
-output <output_file>\
-particles <number of particles>\
-steps <number_of_steps> \
-criteria <evaluation_criteria>\
-gui <gui> “