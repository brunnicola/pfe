import akka.actor.{Actor, ActorRef}

import scala.util.Random

case class LH(dimension: Int, n: Int, swarm: ActorRef, criteria : String)


  extends Actor
  with LHType {


  var matrix: LatinHypercube = Range(0, dimension).toList.map(_ => Random.shuffle((0 until n).toList))

  var pBest: (Double, LatinHypercube) = (eval(matrix),matrix)

  var globalBest: (Double, LatinHypercube) = if(criteria == "maximin")(0 ,Nil) else (Double.MaxValue, Nil)

  val random = new scala.util.Random(42)


  swarm ! COMMUNICATE_PBEST(pBest)

  def ae(lh: LatinHypercube): Double = {
    val l = for (
      i <- 0 until n - 1;
      j <- i + 1 until n
    ) yield 1 / distanceSquared(i, j, lh)
    l.sum
  }

  def eval(lh: LatinHypercube): Double = {
    if(criteria == "maximin") minmax(lh)
    else ae(lh)
  }



  def minmax(lh: LatinHypercube): Double = {
    val l = for (
      i <- 0 until n - 1;
      j <- i + 1 until n
    ) yield Math.sqrt(distanceSquared(i, j, lh))
    l.min
  }


  def swap(column: List[Int], targetColumn: List[Int], i: Int): List[Int] = {
    val indexToSwap = column.indexOf(targetColumn(i))
    column.updated(indexToSwap, column(i)).updated(i, targetColumn(i))
  }

  def move(orginalParticule : LatinHypercube,targetParticule : LatinHypercube): LatinHypercube={
    (0 until dimension)
      .map( i => swap(orginalParticule(i), targetParticule(i), random.nextInt(n))).toList
  }


  def randomSwap(originalParticule: LatinHypercube, index : Int): List[Int] = {
    val r1 = random.nextInt(n)
    val r2 = random.nextInt(n)
    val r3 = random.nextInt(n)
    if(r1 != r2 && r3>n*0.7) originalParticule(index).updated(r1, originalParticule(index)(r2)).updated(r2, originalParticule(index)(r1))
    else originalParticule(index)
  }

  def randomMove(originalParticule: LatinHypercube):LatinHypercube={
    (0 until dimension).map( i => randomSwap(originalParticule, i)).toList
  }

  def hammingDistance(matrix1: LatinHypercube, matrix2: LatinHypercube ): Int ={
    (for(
      i <- matrix1.indices;
      j <- matrix1.head.indices
    )yield matrix1(i)(j) == matrix2(i)(j)).count(!_)
  }


  def distanceSquared(indexLine1: Int, indexLine2: Int, lh: LatinHypercube): Double = {
    (0 until dimension)
      .map(i => Math.pow(lh(i)(indexLine1) - lh(i)(indexLine2), 2))
      .sum
  }

  def updatePBest(lh: LatinHypercube, pBest: (Double, LatinHypercube)):(Double, LatinHypercube) = {

    val newScore= eval(lh)

    if(criteria == "maximin") {
      if(newScore > pBest._1) (newScore, lh)
      else pBest
    }else{
      if(newScore < pBest._1) (newScore, lh)
      else pBest
    }

  }

  override def receive: Receive = {
    case COMMUNICATE_GLOBAL_BEST(globalBestFromSwarm) => {

      globalBest = globalBestFromSwarm

      matrix = randomMove(move(move(matrix,globalBest._2), pBest._2))

      pBest = updatePBest(matrix, pBest)

      sender() ! COMMUNICATE_PBEST(pBest)
    }
  }
}



