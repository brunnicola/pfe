import akka.actor.{ActorRef, ActorSystem, Props}




object Main extends App{

  var numbersOfParticles: Int = 100
  var dimension: Int = 0
  var samples: Int = 0
  var steps: Int = 100
  var GUI: Boolean = false
  var criteria: String = ""
  var output : String = ""

  val usage = """
    sbt run --dimension <dimension> --samples <number_of-samples> --output <fichier_de_sortie>
     --particles <number of particles> --steps <number_of_steps> --criteria <evaluation_criteria> --gui <gui>
  """


  if(args.isEmpty) println(usage)
  args.sliding(2, 2).toList.collect{
      case Array("-dimension", argDimension : String) => dimension = argDimension.toInt
      case Array("-samples", argSamples : String) => samples = argSamples.toInt
      case Array("-steps", argSteps : String) => steps = argSteps.toInt
      case Array("-criteria", argCriteria : String) => criteria = argCriteria
      case Array("-gui", argGui: String) => GUI = argGui == "enable"
      case Array("-particles", argParticles) => numbersOfParticles = argParticles.toInt
      case Array("-output", argOut:String) => output = argOut.mkString
  }

  if(dimension<2) println("Dimension should be superior or equal at 2")
  else if(samples < 2) println("Samples should be superior or equal at 2")
  else if(numbersOfParticles <2) println("Particles should be superiot or equal at 2")
  else if(steps<1) println("Steps should be superior at 1")
  else if(criteria != "maximin" && criteria != "ae" ) println("Criteria should be maximin or ae")
  else{
    // Create the 'PSO' actor system
    val system: ActorSystem = ActorSystem("PSO")

    def swarmProps: Props = Props(Swarm(numbersOfParticles,steps, GUI && dimension==2, output, criteria))

    val swarm: ActorRef = system.actorOf(swarmProps)

    def particuleProps: Props = Props(LH(dimension, samples, swarm, criteria))

    val particles: List[ActorRef] = (0 until numbersOfParticles)
      .map(_ => system.actorOf(particuleProps))
      .toList
  }








}
