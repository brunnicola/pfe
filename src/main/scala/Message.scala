case class COMMUNICATE_PBEST(pbest: (Double, List[List[Int]]))

case class COMMUNICATE_GLOBAL_BEST(gbest: (Double, List[List[Int]]))

object Message {

  val GIVE_ME_YOUR_PB: String = "give_me_your_pb"

}
