import java.io.{File, PrintWriter}

import akka.actor.{Actor, ActorRef, Props}
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
import scalax.chart.XYChart
import scalax.chart.module.XYToolTipGenerators

case class Swarm(numbersOfElements: Int, InNumberOfSteps: Int, swhoChart: Boolean, output: String, criteria:String) extends Actor with LHType with scalax.chart.module.Charting {

  def props: Props = Props[Swarm]

  var numberOfSteps: Int = InNumberOfSteps

  var pBestofParticles: Map[ActorRef, (Double, LatinHypercube)] = Map.empty

  var globalBest: (Double, LatinHypercube) = if(criteria == "maximin")(0 ,Nil) else (Double.MaxValue, Nil)


  def saveSolution(file: String, solution: LatinHypercube): Unit = {
    if (file.nonEmpty) {
      val writer = new PrintWriter(new File(file))
      solution.transpose.foreach(list => {
        writer.write(list.foldLeft("")((acc, s) => acc + s + ",").dropRight(1) + "\n")
      })
      writer.close()
    }
  }

  def plotChart(): Unit = {
    val score = globalBest._1
    val series = new XYSeries(s"Hypercube Latin de score : $score")
    globalBest._2.transpose.foreach(x => series.add(x.head, x.last))
    val plot: XYToolTipGenerators.XYPlot = XYLineChart(series).plot
    plot.setRenderer(new XYLineAndShapeRenderer(false, true))
    val chartScatter: XYChart = XYChart(plot, title = "Generated Latin Hypercube", legend = true)(theme = ChartTheme.Default)
    chartScatter.show()
  }

  def updateGlobalBest(): Unit = {

    if(criteria == "maximin"){
      val bestActualPBest = pBestofParticles.valuesIterator.maxBy(_._1)

      if (bestActualPBest._1 > globalBest._1) {
        val score = bestActualPBest._1
        val matrice = bestActualPBest._2
        println(score)
        globalBest = bestActualPBest
      }
    }else{
      val bestActualPBest = pBestofParticles.valuesIterator.minBy(_._1)
      if (bestActualPBest._1 < globalBest._1) {
        val score = bestActualPBest._1
        val matrice = bestActualPBest._2
        println(score)
        globalBest = bestActualPBest
      }
    }

  }



  override def receive: Receive = {

    case COMMUNICATE_PBEST(pbest) =>

      pBestofParticles = pBestofParticles + (sender() -> pbest)
      if (pBestofParticles.size == numbersOfElements) {

        updateGlobalBest()

        if (numberOfSteps != 0) {
          pBestofParticles.keysIterator
            .foreach(particle => particle ! COMMUNICATE_GLOBAL_BEST(globalBest))
          pBestofParticles = Map.empty
          numberOfSteps = numberOfSteps - 1

        } else {

          if (swhoChart) plotChart()

          saveSolution(output, globalBest._2)

          context.system.terminate()
        }

      }

  }
}
