import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit}
import org.scalatest._

class LHSpec(_system: ActorSystem) extends TestKit(_system)
  with Matchers
  with WordSpecLike
  with BeforeAndAfterAll {

  def this() = this(ActorSystem("LHSpec"))

  override def afterAll: Unit = {
    shutdown(system)
  }

 /* val swarmRef = TestActorRef(Swarm(10,100, false))

  val swarmActor: Swarm = swarmRef.underlyingActor

  val lhRef = TestActorRef(LH(2, 5, swarmActor.self))

  val lhActor: LH = lhRef.underlyingActor


  "A LH" must {
    "have the correct dimension" in {
      lhActor.matrix.size should be(2)
    }
  }

  "A LH" must {
    "have the correct number of sample" in {
      lhActor.matrix.foreach(x => x.size should be(5))
    }
  }

  "A LH" must {
    "have unique elements in column" in {
      lhActor.matrix.foreach(x => x.size should be(x.toSet.size))
    }
  }

  "The LH distance Between line 0 and line 1" must {
    "be 2" in {
      val lh2 = TestActorRef(LH(2, 2, swarmRef)).underlyingActor
      lh2.matrix = List(List(0, 1), List(0, 1))
      lh2.distanceSquared(0, 1, lh2.matrix) should be(2)
    }
  }

  "The LH AE 1" must {
    "be 1.125" in {
      val lh2 = TestActorRef(LH(2, 3, swarmRef)).underlyingActor
      lh2.matrix = List(List(0, 1, 2), List(0, 1, 2))
      lh2.eval(lh2.matrix) should be(1.125)
    }
  }

  "The LH Hamming distance " must {
    "be 1.125" in {
      val lh2 = TestActorRef(LH(2, 2, swarmRef)).underlyingActor
      lh2.matrix = List(List(0, 1), List(0, 1))

      lh2.globalBest = (0, List(List(0, 1), List(1, 0)))
      lh2.hammingDistance(lh2.matrix, lh2.globalBest._2) should be(2)
    }
  }

  "The LH " must {
    "swap the first element in each column" in {
      val lh2 = TestActorRef(LH(1, 4, swarmRef)).underlyingActor
      lh2.matrix = List(List(0, 1, 2, 3))

      lh2.globalBest = (0, List(List(3, 2, 1, 0)))
      lh2.move(lh2.matrix,lh2.globalBest._2) should be(List(List(3,1,2,0)))
    }
  }


  "The LH22 " must {
    "swap the first element in each column" in {
      val lh2 = TestActorRef(LH(2, 10, swarmRef)).underlyingActor
     // lh2.display()

      print(lh2.randomMove(lh2.matrix))
    }
  }*/

}
